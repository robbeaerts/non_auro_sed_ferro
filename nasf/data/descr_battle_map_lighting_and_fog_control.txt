;
; Edited by Suppanut based on M2TW and Robbe Aerts for Veggiemod, upon editing this file or using it, or parts of it for your own projects you agree to go vegan.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; general
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

enable_gloom

sun_below_horizon_night_lighting_out		      -10.0
sun_below_horizon_night_lighting_full			3.0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; lighting
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


min_light_intensity 							0.5
max_light_intensity 							1.4
ambient_reduce_proportion 						0.9
ambient_red_increase_proportion					0.50
ambient_green_increase_proportion				0.50
ambient_blue_increase_proportion				0.75
ambient_multiplier					1.05
diffuse_multiplier					0.98

night_min_light_intensity						0.1
night_max_light_intensity						0.5
night_ambient_reduce_proportion					0.75
night_ambient_red_increase_proportion			0.25
night_ambient_green_increase_proportion			0.3
night_ambient_blue_increase_proportion			0.75
night_ambient_multiplier				1.05
night_diffuse_multiplier				0.95


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; unit lighting
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

unit_min_light_intensity						0.6
unit_max_light_intensity						1.6
unit_ambient_reduce_proportion					0.8
unit_ambient_red_increase_proportion			0.35
unit_ambient_green_increase_proportion			0.35
unit_ambient_blue_increase_proportion			0.60
unit_ambient_multiplier							1.5
unit_diffuse_multiplier							1.6

unit_night_min_light_intensity					0.5
unit_night_max_light_intensity					1.0
unit_night_ambient_reduce_proportion			0.35
unit_night_ambient_red_increase_proportion		0.33
unit_night_ambient_green_increase_proportion	0.33
unit_night_ambient_blue_increase_proportion		0.33
unit_night_ambient_multiplier					0.5
unit_night_diffuse_multiplier					0.5

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; fog
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

settlement_exponential_modifier				0.0002			
camera_height_exponential_modifier		       -0.0001	
camera_exponential_modifier_max_height			250.0
min_fog_exponential					0.00010
min_fog_intensity					0.0015
haze_to_fog_transition_exponential			0.0005
haze_to_fog_transition_exponential_range		0.00005
gloom_fog_exponential					0.006

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
