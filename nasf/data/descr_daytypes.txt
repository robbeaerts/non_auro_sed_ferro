;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Daytypes description file
;
; In two parts overall. Part 1 describes different weather patterns which can be referenced 
; from a given type of day.
;
;	weatherevent <type_id>
;			cloud			<type>	[ <colour: r g b> ]
;			precipitate		<type>	<level>
;			wind			<level>
;			fog				<level> <density> [ <colour: r g b> ]
;
; Notes:
;
; - It isn`t necessary to define all the elements.  The first event in the list will provide a base setting which 
;	subsequent events can override. 
;
; - Fog and cloud colours are optional. If no colour is specified the cloud colour is assumed to be white and the fog colour is assumed to be (0.8 0.8 1).
;
; - The intensity of the fog colour will be adjusted with the time of day so the fog darkens as the sun sets and is black at night.
;
; - The in game fog colour moves from the current haze colour to the current adjusted fog colour from fog density 0.00035 to 0.0004
;
; - Some reasonable values for fog density (haze->heavy)  0.00005, 0.0005, 0.001, 0.002 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Cloud types			light, medium, heavy
; Precipitate types:	none, rain, snow, hail
; Precipitate levels:	dry, drizzle, light, heavy, torrential
; Wind levels:			calm, breezes, wind, gusts, gales
; Fog levels:			clear, haze, light, heavy
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

weatherevent medicane
		cloud			heavy			0.85	0.85	0.85	
		precipitate		rain torrential
		wind			gales
		fog			clear		0.00004	0.35	0.35	0.4	

weatherevent rain_storm
		cloud			heavy			0.89	0.89	0.89	
		precipitate		rain heavy
		wind			gusts
		fog			clear		0.00004	0.35	0.35	0.4	

weatherevent rain_summer_shower
		cloud			medium			0.95	0.95	0.95
		precipitate		rain light
		wind			breezes
		fog				haze		0.0008			

weatherevent rain_drizzle
		cloud			medium		
		precipitate		rain drizzle
		wind			wind
		fog			light		0.00126	0.6	0.65	0.7

weatherevent hail_shower
		cloud			medium			0.95	0.95	0.95
		precipitate		hail heavy
		wind			gusts
		fog			clear		0.00003	0.35	0.35	0.4

weatherevent snow_falling
		cloud			medium		
		precipitate		snow light
		wind			gusts
		fog			heavy		0.00126	0.9	0.9	1.0

weatherevent blizzard
		cloud			heavy			0.95	0.95	0.95
		precipitate		snow torrential
		wind			gales
		fog			heavy		0.00126	0.8	0.8	0.9

weatherevent calm
		cloud			light		
		precipitate		none    
		wind			calm
		fog			clear		0.00001	0.35	0.35	0.4	

weatherevent hazy
		cloud			light		
		precipitate		none    
		wind			calm
		fog			clear		0.00002	0.35	0.35	0.4		
		
weatherevent desert_haze
		cloud			light		
		precipitate		none    
		wind			breezes
		fog			haze		0.00035	1	1	1		

weatherevent light_fog
		cloud			light		
		precipitate		none    
		wind			calm
		fog			light		0.00126	0.62	0.63	0.7

weatherevent heavy_fog
		cloud			light		
		precipitate		none
		wind			calm
		fog			heavy		0.00126	0.64	0.64	0.67

weatherevent dust_storm
		cloud		   	light			0.93	0.94	0.91
		precipitate		dust torrential
		wind			gusts
		fog			clear		0.00004	0.37	0.36	0.34

				

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	Part 2 : daytypes are different kind of days which can occur in different climates
;	and seasons, allowing control over the lighting settings of the game in widely
;	differing environments
;
;	daytype {type_id}
;
;		climates		{climates that this daytype is applicable to - space seperated list}
;		seasons			{seasons that this daytype is applicable to}
;		weatherevents	{weather events that can occur coupled with weight indicating chance of happening - space seperated list}
;		next_day		{space seperated list of days that can follow this type}
;
;		event
;			time			{24hr value for time event starts}
;			sun_colour		r g b
;			sunlight		{diffuse}
;			moonlight		{full diffuse} {new diffuse}
;			haze_colour		r g b
;			cloud_colour	{east r g b} {west r g b}
;
;		event
;			....
;	daytype 
;		....
;
; Notes:
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Climates:				As described in descr_climates.txt	
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

daytype	temperate_summer

	climates		temperate_grassland_fertile temperate_grassland_infertile temperate_forest_deep highland sub_arctic temperate_forest_open swamp
	seasons			summer 
	weatherevents		calm 0.25, hazy 0.4, light_fog 0.15, rain_summer_shower 0.13, rain_storm 0.05, hail_shower 0.02	
	next_day		temperate_summer 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	temperate_winter

	climates		temperate_forest_open, temperate_forest_deep
	seasons			winter
	weatherevents		light_fog 0.1, heavy_fog 0.05, rain_drizzle 0.15, calm 0.35, snow_falling 0.05, hazy 0.2, rain_storm 0.05, hail_shower 0.05
	next_day		temperate_winter 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	nordic_summer

	climates		sub_arctic ; , mediterranean
	seasons			summer 
	weatherevents		calm 0.25, hazy 0.4, light_fog 0.15, rain_summer_shower 0.13, rain_storm 0.05, hail_shower 0.02	
	next_day		temperate_summer 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	cold_winter

	climates		swamp, alpine, highland
	seasons			winter
	weatherevents		heavy_fog 0.1, snow_falling 0.15, calm 0.3, light_fog 0.2, hazy 0.15
	next_day		cold_winter 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	arctic_winter

	climates		sub_arctic ; , mediterranean
	seasons			winter
	weatherevents		heavy_fog 0.1, snow_falling 0.2, blizzard 0.05, calm 0.25, light_fog 0.15, hazy 0.15
	next_day		arctic_winter 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	mediterranean_forest_summer

	climates		test_climate
	seasons			summer
	weatherevents		calm 0.75, hazy 0.05, light_fog 0.05, heavy_fog 0.05, rain_summer_shower 0.02, rain_storm 0.02, hail_shower 0.01
	next_day		mediterranean_forest_summer 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	0.99
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0
		
		
daytype	mediterranean_forest_winter

	climates		test_climate
	seasons			winter
	weatherevents		calm 0.35, hazy 0.1, light_fog 0.1, heavy_fog 0.1, rain_storm 0.1, rain_drizzle 0.1, medicane 0.1, hail_shower 0.05
	next_day		mediterranean_forest_winter 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	mediterranean_summer

	climates		grassland_fertile, semi_arid
	seasons			summer
	weatherevents		calm 0.9, rain_summer_shower 0.02, rain_storm 0.02, hail_shower 0.01
	next_day		mediterranean_summer 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	0.99
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0
		
		
daytype	mediterranean_winter

	climates		grassland_fertile, semi_arid
	seasons			winter
	weatherevents		calm 0.55, hazy 0.05, light_fog 0.05, heavy_fog 0.05, rain_storm 0.1, rain_drizzle 0.05, medicane 0.1, hail_shower 0.05
	next_day		mediterranean_winter 1.0										

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.23	0.25	0.26
		cloud_colour		0.72	0.67	0.62		0.74	0.74	0.74

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.33	0.37	0.39
		cloud_colour		0.93	0.87	0.81		0.99	0.99	0.99

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	1.0	1.0
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.49	0.6	0.66	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.34	0.38	0.4
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.15	0.17	0.18
		cloud_colour		0.58	0.57	0.59		0.84	0.62	0.49

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	semi_arid_summer

	climates		temperate_grassland_infertile, rocky_desert ; , volcanic
	seasons			summer
	weatherevents		dust_storm 0.1, desert_haze 0.6, calm 0.3
	next_day		semi_arid_summer 1.0

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.37	0.34	0.38
		cloud_colour	0.84	0.60	0.39		0.5		0.5		0.6

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.44	0.43	0.52
		cloud_colour	0.99	0.71	0.58		0.8		0.7		0.8

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	0.99	0.97
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.47	0.45	0.65	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.9		0.65	0.55
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.3		0.25	0.4
		cloud_colour	0.6		0.4		0.6			0.95	0.7		0.45

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0
		
daytype	semi_arid_winter

	climates		temperate_grassland_infertile, rocky_desert
	seasons			winter
	weatherevents		dust_storm 0.05, desert_haze 0.4, calm 0.5, rain_drizzle 0.01, rain_storm 0.02, medicane 0.01, hail_shower 0.01
	next_day		semi_arid_winter 1.0

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.37	0.34	0.38
		cloud_colour	0.84	0.60	0.39		0.5		0.5		0.6

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.44	0.43	0.52
		cloud_colour	0.99	0.71	0.58		0.8		0.7		0.8

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	0.99	0.97
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.47	0.45	0.65	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.9		0.65	0.55
		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.3		0.25	0.4
		cloud_colour	0.6		0.4		0.6			0.95	0.7		0.45

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0
		
;daytype	semi_arid_highland_winter
;
;	climates		volcanic
;	seasons			winter
;	weatherevents		dust_storm 0.05, desert_haze 0.3, calm 0.5, hazy 0.05, light_fog 0.05, snow_falling 0.02, rain_drizzle 0.01, hail_shower 0.01, rain_storm 0.01
;	next_day		semi_arid_highland_winter 1.0
;
;; night
;	event 
;		time			0
;	
;		sun_colour		1	.8	0.6
;		sunlight		0.95	.9	0.75
;		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
;		haze_colour		0.08	0.08	0.1
;		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0
;
;; sunrise
;	event
;		time			7
;
;		sun_colour		1	0.75	0.5
;		sunlight		0.9	0.8	0.65
;		moonlight		0.5	0.5	1		0.75	0.1	0.2
;		haze_colour		0.37	0.34	0.38
;		cloud_colour	0.84	0.60	0.39		0.5		0.5		0.6
;
;; straight after sunrise
;	event
;		time			8
;
;		sun_colour		1.0	0.9	0.75
;		sunlight		1	0.9	0.75
;		moonlight		.5	.5	1		0.75	0.1	0.2
;		haze_colour		0.44	0.43	0.52
;		cloud_colour	0.99	0.71	0.58		0.8		0.7		0.8
;
;; midday
;	event
;		time			12
;
;		sun_colour		1.0	0.95	0.9
;		sunlight		1.0	0.99	0.97
;		moonlight		.5	.5	1		0.75	0.1	0.2
;		haze_colour		0.47	0.45	0.65	
;		cloud_colour		1	1	1		1	1	1
;
;;afternoon
;	event
;		time			16
;
;		sun_colour		1.0	0.85	0.65
;		sunlight		1.0	0.86	0.79
;		moonlight		.5	.5	1		0.75	0.1	0.2
;		haze_colour		0.9		0.65	0.55
;		cloud_colour		0.9	0.89	0.91		0.95	0.85	0.75
;
;; sun going down
;	event 
;		time			17
;	
;		sun_colour		1.0	0.62	0.43
;		sunlight		1.0	0.71	0.64
;		moonlight		.7	.65	1		0.75	0.1	0.2
;		haze_colour		0.3		0.25	0.4
;		cloud_colour	0.6		0.4		0.6			0.95	0.7		0.45
;
;; night
;	event 
;		time			21
;	
;		sun_colour		1.0	0.8	0.6
;		sunlight		0.95	.9	0.75
;		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
;		haze_colour		0.13	0.13	0.15
;		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	desert_summer

	climates		sandy_desert
	seasons			summer winter
	weatherevents		dust_storm 0.15, desert_haze 0.85
	next_day		desert_summer 1.0

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.3		0.25	0.35
		cloud_colour	0.9		0.5		0.25		0.6		0.5		0.5

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.4		0.35	0.45
		cloud_colour	0.9		0.7		0.7			0.8		0.7		0.8

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	0.98	0.95
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.8		0.8		0.9	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.65	0.4		0.25
		cloud_colour	0.7		0.6		0.5			1.0		0.6		0.2

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.5		0.25	0.15
		cloud_colour	0.6		0.5		0.4			1.0		0.4		0.0

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0
		
daytype	desert_winter

	climates		sandy_desert
	seasons			winter
	weatherevents		dust_storm 0.1, desert_haze 0.9
	next_day		desert_winter 1.0

; night
	event 
		time			0
	
		sun_colour		1	.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.08	0.08	0.1
		cloud_colour		0.1	0.0	0.0		0.0	0.0	0.0

; sunrise
	event
		time			7

		sun_colour		1	0.75	0.5
		sunlight		0.9	0.8	0.65
		moonlight		0.5	0.5	1		0.75	0.1	0.2
		haze_colour		0.3		0.25	0.35
		cloud_colour	0.9		0.5		0.25		0.6		0.5		0.5

; straight after sunrise
	event
		time			8

		sun_colour		1.0	0.9	0.75
		sunlight		1	0.9	0.75
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.4		0.35	0.45
		cloud_colour	0.9		0.7		0.7			0.8		0.7		0.8

; midday
	event
		time			12

		sun_colour		1.0	0.95	0.9
		sunlight		1.0	0.98	0.95
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.8		0.8		0.9	
		cloud_colour		1	1	1		1	1	1

;afternoon
	event
		time			16

		sun_colour		1.0	0.85	0.65
		sunlight		1.0	0.86	0.79
		moonlight		.5	.5	1		0.75	0.1	0.2
		haze_colour		0.65	0.4		0.25
		cloud_colour	0.7		0.6		0.5			1.0		0.6		0.2

; sun going down
	event 
		time			17
	
		sun_colour		1.0	0.62	0.43
		sunlight		1.0	0.71	0.64
		moonlight		.7	.65	1		0.75	0.1	0.2
		haze_colour		0.5		0.25	0.15
		cloud_colour	0.6		0.5		0.4			1.0		0.4		0.0

; night
	event 
		time			21
	
		sun_colour		1.0	0.8	0.6
		sunlight		0.95	.9	0.75
		moonlight		0.20	0.20	0.30		0.20 	0.045	0.05
		haze_colour		0.13	0.13	0.15
		cloud_colour		0.0	0.0	0.0		0.1	0.0	0.0

daytype	sound_test

	climates		test_climate temperate_grassland_fertile temperate_grassland_infertile temperate_forest_open temperate_forest_deep swamp highland alpine sub_arctic semi_arid desert rocky_desert
	seasons			summer winter
	weatherevents	medicane 0.076, rain_storm 0.077, rain_summer_shower 0.077, desert_haze 0.077, rain_drizzle 0.077, hail_shower 0.077, snow_falling 0.077, calm 0.077, hazy 0.077, light_fog 0.077 heavy_fog 0.077 dust_storm 0.077 blizzard 0.077
	next_day		sound_test 1.0
	test										

; 
	event
		time			0

		sun_colour		1.0	0.75	0.55
		sunlight		0.75	0.65	0.5
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.3	0.3	0.3	
		cloud_colour		0	0	0		0	0	0

	event
		time			7

		sun_colour		1.0	0.75	0.55
		sunlight		1.05	0.75	0.5
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.63	0.62	0.55	
		cloud_colour		1.0	0.5	0.3		0.6	0.6	0.55

	event
		time			8

		sun_colour		1.0	0.85	0.6
		sunlight		0.93	0.87	0.8
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.70	0.71	0.69	
		cloud_colour		1.0	0.75	0.55		0.7	0.7	0.65

	event
		time			12

		sun_colour		1.0	0.9	0.7
		sunlight		1.0	1.0	1.0
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.83	0.84	0.80	
		cloud_colour		1.0	1.0	1.0		1.0	1.0	1.0

	event
		time			16

		sun_colour		1.0	0.8	0.65
		sunlight		0.97	0.83	0.8
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.70	0.71	0.69	
		cloud_colour		0.7	0.65	0.7		1.0	0.75	0.55

	event
		time			18

		sun_colour		1.0	0.7	0.6
		sunlight		1.15	0.65	0.5
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.63	0.62	0.55	
		cloud_colour		0.6	0.55	0.6		1.0	0.4	0.4

	event
		time			21

		sun_colour		1.0	0.7	0.6
		sunlight		0.75	0.65	0.5
		moonlight		.45	.45	.6		.45	.45	.6
		haze_colour		0.3	0.3	0.3	
		cloud_colour		0	0	0		0	0	0